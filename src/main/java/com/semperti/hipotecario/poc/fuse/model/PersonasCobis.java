package com.semperti.hipotecario.poc.fuse.model;

import java.io.Serializable;

import java.util.List;
import java.util.ArrayList;

public class PersonasCobis implements Serializable {
	List<PersonaCobis> personas = new ArrayList<PersonaCobis>();
	public PersonasCobis() {}
	
	public PersonasCobis(List<PersonaCobis> personas) {
		this.personas= personas;
	}
	public List<PersonaCobis> getPersonas() {
		return personas;
	}

	public void setPersonas(List<PersonaCobis> personas) {
		this.personas = personas;
	}
}

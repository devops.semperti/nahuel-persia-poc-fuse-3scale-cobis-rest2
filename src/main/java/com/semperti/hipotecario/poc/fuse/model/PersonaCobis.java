package com.semperti.hipotecario.poc.fuse.model;

import java.util.ArrayList;

public class PersonaCobis extends Persona{

	protected ArrayList<PrestamoPatrimonial> prestamosPatrimoniales = null;
	protected ArrayList<TarjetaCreditoPatrimonial> tarjetasCreditoPatrimoniales = null;

	/**
	 * Score VLC del cliente
	 * @return valorLealtadCliente
	 **/
	
	public ArrayList<PrestamoPatrimonial> getPrestamosPatrimoniales() {
		return prestamosPatrimoniales;
	}
	
	public void setPrestamosPatrimoniales(ArrayList<PrestamoPatrimonial> prestamosPatrimoniales) {
		this.prestamosPatrimoniales = prestamosPatrimoniales;
	}

	/**
	 * Todas las tarjetas de credito declaradas por la persona
	 * @return tarjetasCreditoPatrimoniales
	 **/
	public ArrayList<TarjetaCreditoPatrimonial> getTarjetasCreditoPatrimoniales() {
		return tarjetasCreditoPatrimoniales;
	}

	public void setTarjetasCreditoPatrimoniales(ArrayList<TarjetaCreditoPatrimonial> tarjetasCreditoPatrimoniales) {
		this.tarjetasCreditoPatrimoniales = tarjetasCreditoPatrimoniales;
	}
	
}
